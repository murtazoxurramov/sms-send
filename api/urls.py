from django.urls import path, include
from .views import ListCreateClientAPIView, RUDClientAPIView, DistributionListCreateAPIView
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'clients', ListCreateClientAPIView)
router.register(r'messages', RUDClientAPIView)
router.register(r'distribution', DistributionListCreateAPIView)

urlpatterns = [
    path('', include(router.urls)),
]
